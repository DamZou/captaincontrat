class Transfer

	def transfers(price, captain_service_fee, lawyer_service_fee)
		transfers = []
		[0, 1, 2, 3].each do |i|
			if i == 0 
				tab = ["pay_in", "client", "order", price]
			elsif i == 1 
				tab = ["transfer", "order", "lawyer", price - captain_service_fee]
			elsif i == 2 
				tab = ["transfer", "lawyer", "captain", if lawyer_service_fee < 0 then 0 else lawyer_service_fee end]
			elsif i == 3 
				tab = ["transfer", "order", "captain", captain_service_fee]
			end
			transfers.push({:type => tab[0], 
				:from => tab[1], 
				:to => tab[2], 
				:amount => tab[3]
				})
		end
		transfers
	end

end