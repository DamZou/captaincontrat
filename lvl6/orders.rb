class Order
	attr_accessor :id

	def initialize(id)
		@id = id	
	end

	def price(json, order_type)
		price = 0
		promotion = 0
		json["order_" + order_type].each do |order|
			if order["order_id"] == self.id && order_type == "documents"
				if order["lawyer_id"] != nil
					price += json["lawyers"][order["lawyer_id"].to_i - 1][order_type][order["document_id"].to_i - 1]["price"]
					promotion += Promotion.new(order["promotion_id"], json["lawyers"][order["lawyer_id"].to_i - 1][order_type][order["document_id"].to_i - 1]["price"]).promotion(json)
				else
					price += json[order_type][order["document_id"].to_i - 1]["price"]
					promotion += Promotion.new(order["promotion_id"], json[order_type][order["document_id"].to_i - 1]["price"]).promotion(json)
				end
			elsif order["order_id"] == self.id && order_type == "products"
				if order["lawyer_id"] != nil
					price += json["lawyers"][order["lawyer_id"].to_i - 1][order_type][order["product_id"].to_i - 1]["price"]
					promotion += Promotion.new(order["promotion_id"], json["lawyers"][order["lawyer_id"].to_i - 1][order_type][order["product_id"].to_i - 1]["price"]).promotion(json)
				else
					price += json[order_type][order["product_id"].to_i - 1]["price"]
					promotion += Promotion.new(order["promotion_id"], json[order_type][order["product_id"].to_i - 1]["price"]).promotion(json)
				end	
			end
		end
		{:price => price, :promotion => promotion}
	end

	def service_fee(json, order_type)
		service_fee = {:captain => 0, :lawyer => 0}
		json["order_" + order_type].each do |order|
			if order["order_id"] == self.id && order_type == "documents" && order["lawyer_id"] != nil
				service_fee[:captain] += json["lawyers"][order["lawyer_id"].to_i - 1][order_type][order["document_id"].to_i - 1]["service_fee"]
			elsif order["order_id"] == self.id && order_type == "products" && order["lawyer_id"] != nil
				service_fee[:captain] += json["lawyers"][order["lawyer_id"].to_i - 1][order_type][order["product_id"].to_i - 1]["service_fee"]
			elsif order["order_id"] == self.id && order_type == "documents"
				service_fee[:lawyer] += json[order_type][order["document_id"].to_i - 1]["service_fee"]
			elsif order["order_id"] == self.id && order_type == "products"
				service_fee[:lawyer] += json[order_type][order["product_id"].to_i - 1]["service_fee"]
			end
		end
		service_fee
	end
end