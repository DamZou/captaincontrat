class Promotion
	attr_accessor :id, :price

	def initialize(id, price)
		@id = id	
		@price = price
	end

	def promotion(json)
		if self.id == nil
			0
		elsif json["promotions"][self.id.to_i - 1]["reduction"] == 0
			json["promotions"][self.id.to_i - 1]["reduction_fixe"]
		elsif json["promotions"][self.id.to_i - 1]["reduction_fixe"] == 0
			(json["promotions"][self.id.to_i - 1]["reduction"] * self.price / 100).round(0)
		end
	end

end