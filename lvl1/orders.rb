class Order
	attr_accessor :document_id

	def initialize(document_id)
		@document_id = document_id	
	end

	def price(json)
		json["documents"][self.document_id.to_i - 1]["price"]
	end

end