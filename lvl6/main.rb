require "json"
require "./orders.rb"
require "./promotions.rb"
require "./transfers.rb"

def parse_json(file)
  json = File.read(file)
  data_hash = JSON.parse(json)
end

def orders(datas)
	orders = []
	datas["orders"].each do |order|
		new_order = Order.new(order["id"])
		document_price = new_order.price(datas, "documents")
		product_price = new_order.price(datas, "products")
		document_service_fee = new_order.service_fee(datas, "documents")
		product_service_fee = new_order.service_fee(datas, "products")
		promotion = document_price[:promotion] + product_price[:promotion]
		captain_service_fee = document_service_fee[:captain] + product_service_fee[:captain]
		lawyer_service_fee = document_service_fee[:lawyer] + product_service_fee[:lawyer] - promotion
		if lawyer_service_fee < 0
			lawyer_service_fee = 0 
		end
		price_with_promotion = document_price[:price] + product_price[:price] - promotion
		new_transfer = Transfer.new()
		orders.push({:id => order["id"], 
					 :price => if price_with_promotion < 0 then 0 else price_with_promotion end,
					 :transfers => new_transfer.transfers(price_with_promotion, captain_service_fee, lawyer_service_fee)
					})
	end
	orders
end

def verifications(orders, results)
	i = 0
	orders.each do |order|
		if order == results["orders"][i]
			puts "correspondance ok"
			puts "order"
			puts order
			puts "result"
			puts results["orders"][i]
		else
			puts "pas de correspondance"
			puts "order"
			puts order
			puts "result"
			puts results["orders"][i]
		end
		i += 1
	end
end

datas = parse_json('data.json')
results = parse_json('output.json')
orders = JSON.parse(orders(datas).to_json)
verifications(orders, results)