require "json"
require "./orders.rb"
require "./promotions.rb"

def parse_json(file)
  json = File.read(file)
  data_hash = JSON.parse(json)
end

def orders(datas)
	orders = []
	datas["orders"].each do |order|
		new_order = Order.new(order["document_id"])
		price = new_order.price(datas)
		new_promotion = Promotion.new(order["promotion_id"], price)
		promotion = new_promotion.promotion(datas)
		price_with_promotion = price - promotion
		orders.push({:id => order["id"], 
					 :price => if price_with_promotion < 0 then 0 else price_with_promotion end
					})
	end
	orders
end

def verifications(orders, results)
	i = 0
	orders.each do |order|
		if order == results["orders"][i]
			puts "correspondance ok"
			puts "order"
			puts order
			puts "result"
			puts results["orders"][i]
		else
			puts "pas de correspondance"
			puts "order"
			puts order
			puts "result"
			puts results["orders"][i]
		end
		i += 1
	end
end

datas = parse_json('data.json')
results = parse_json('output.json')
orders = JSON.parse(orders(datas).to_json)
verifications(orders, results)