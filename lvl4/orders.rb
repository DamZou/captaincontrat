class Order
	attr_accessor :id

	def initialize(id)
		@id = id	
	end

	def price(json, type)
		price = 0
		json["order_" + type].each do |order|
			if order["order_id"] == self.id && type == "documents"
				price += json[type][order["document_id"].to_i - 1]["price"]
			elsif order["order_id"] == self.id && type == "products"
				price += json[type][order["product_id"].to_i - 1]["price"]
			end
		end
		price
	end

	def service_fee(json, type)
		service_fee = 0
		json["order_" + type].each do |order|
			if order["order_id"] == self.id && type == "documents"
				service_fee += json[type][order["document_id"].to_i - 1]["service_fee"]
			elsif order["order_id"] == self.id && type == "products"
				service_fee += json[type][order["product_id"].to_i - 1]["service_fee"]
			end
		end
		service_fee
	end
end